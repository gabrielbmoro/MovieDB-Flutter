# Welcome!

- This repository provides a Flutter project that uses the library [Movie DB API](https://www.themoviedb.org)

## Teaser

![Flutter Version](img/teaser_flutter_version.gif)
