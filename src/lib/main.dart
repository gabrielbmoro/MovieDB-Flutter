import 'package:flutter/material.dart';
import 'presentation/common/colors.dart';
import 'presentation/screens/homeScreen/home_page_screen.dart';

const String DATA_BASE_NAME = 'MovieDBAppDataBase';

void main() => runApp(MovieDBApi());

class MovieDBApi extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Movie DB App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        primaryColor: colorPrimary,
        primaryColorDark: colorPrimaryDark,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: HomePageScreenWidget(),
    );
  }
}
